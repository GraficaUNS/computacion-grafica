﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK; //La matematica
using OpenTK.Graphics.OpenGL;
using gl = OpenTK.Graphics.OpenGL.GL;
using CGUNS.Shaders;
using CGUNS.Cameras;
using CGUNS.Meshes;
using CGUNS.Parsers;
using System.Drawing.Imaging;

namespace CGUNS.Meshes.FaceVertexList
{

    public partial class Map
    {
        internal class clave
        {
            private int valor;
            private String key;
            private String path;
            public clave(int v, String p,String k)
            {
                key = k;
                valor = v;
                path = p;
            }

            public int getValor()
            {
                return valor;
            }

            public String getPath()
            {
                return path;
            }

            public bool iskey(String k)
            {
                return k.Equals(key);
            }

        }
        private List<clave> lista;
        public Map()
        {
            lista = new List<clave>();
        }

        public int find(String key)
        {
            bool verd = false;
            int i = 0;
            while (!verd && i < lista.Count)
            {
                if (lista[i].iskey(key))
                    verd = true;
                else
                    i++;
            }
            if (i < lista.Count)
                return lista[i].getValor();
            else
                return -1;
        }

        public String findPath(String key)
        {
            bool verd = false;
            int i = 0;
            while (!verd && i < lista.Count)
            {
                if (lista[i].iskey(key))
                    verd = true;
                else
                    i++;
            }
            if (i < lista.Count)
                return lista[i].getPath();
            else
                return null;
        }

        public void setClave(int valor, String path, String key)
        {
            lista.Add(new clave(valor,path ,key));
        }

        public int Count()
        {
            return lista.Count;
        }

    }

}
