﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using gl = OpenTK.Graphics.OpenGL.GL;

using OpenTK; //La matematica
using OpenTK.Graphics.OpenGL;

using System.Drawing.Imaging;
using System.Drawing;

namespace CGUNS.Textures {
    public class TextureCube
    {

        private int textureID;
        public TextureCube(String posX, String negX, String posY, String negY, String posZ, String negZ)
        {
            //TextureTarget.TextureCubeMapNegativeX
            int texId = GL.GenTexture();



            // glTexStorage2D(GL_TEXTURE_CUBE_MAP, 1, GL_RGBA8, 256, 256);
            // gl.TexStorage2D(TextureTarget2d.TextureCubeMap, 1, SizedInternalFormat.Rgba8, 1024, 1024);

            GL.BindTexture(TextureTarget.TextureCubeMap, texId);

            CargarTextura(posX, TextureTarget.TextureCubeMapPositiveX, texId);
            CargarTextura(negX, TextureTarget.TextureCubeMapNegativeX, texId);
            CargarTextura(posY, TextureTarget.TextureCubeMapPositiveY, texId);
            CargarTextura(negY, TextureTarget.TextureCubeMapNegativeY, texId);
            CargarTextura(posZ, TextureTarget.TextureCubeMapPositiveZ, texId);
            CargarTextura(negZ, TextureTarget.TextureCubeMapNegativeZ, texId);


            GL.TexParameter(TextureTarget.TextureCubeMap,
            TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap,
            TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap,
            TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap,
            TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap,
            TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);

            textureID = texId;

        }

        private void CargarTextura(String imagenTex, TextureTarget target, int texId)
        {

            //genera identificador para el buffer de textura
            try
            {

                //pepara buffer de textura
                Bitmap bitmap = new Bitmap(Image.FromFile(imagenTex));
                BitmapData data = bitmap.LockBits(new Rectangle(0, 0,bitmap.Width, bitmap.Height),
                                                    ImageLockMode.ReadOnly,
                                                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                GL.TexImage2D(target,0, PixelInternalFormat.Rgba, data.Width, data.Height,0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
                /*GL.TexSubImage2D(target, 0,,0,
                PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                PixelType.UnsignedByte, data.Scan0);
                */
                bitmap.UnlockBits(data);

            }
            catch (System.IO.FileNotFoundException e)
            {
                Console.WriteLine("Cargar Textura No File: "+ imagenTex);
            }

        }

        public int getTextureId()
        {
            return textureID;
        }
    }
}
