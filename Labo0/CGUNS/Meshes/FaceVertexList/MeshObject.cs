﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK; //La matematica
using OpenTK.Graphics.OpenGL;
using gl = OpenTK.Graphics.OpenGL.GL;
using CGUNS.Shaders;
using CGUNS.Cameras;
using CGUNS.Meshes;
using CGUNS.Parsers;
using System.Drawing.Imaging;
using BulletSharp;

namespace CGUNS.Meshes.FaceVertexList
{

    public class MeshObject
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="mtl"></param>
        /// <param name="posicionInicial"></param>
        /// <param name="texturado"></param>
        public MeshObject(String file, String mtl, Vector3 posicionInicial, bool texturado)
        {
            if(mtl != null)
                objeto = CGUNS.Parsers.ObjFileParser.parseFile(file);
            else
                objeto = CGUNS.Parsers.ObjFileParser.parseFileSinMTL(file);

            position = posicionInicial;
            tieneTextura = texturado;
            CGUNS.Parsers.ObjFileParser.setLists(this);
            if (mtl != null)
                CGUNS.Parsers.MtlFileParser.parseFile(objeto, mtl);
        }

        private List<Vector3> vertexList;

        //matrices
        protected Matrix4 modelMat;
        protected Matrix4 modelMatFisica;
        protected Matrix4 viewMatrix;


        //vectores
        protected Vector3 position;


        protected bool tieneTextura;

        protected List<CGUNS.Meshes.FaceVertexList.FVLMesh> objeto;

        private Map mapeo = new Map();

        protected float radio = 0.1f;

        public void Build(ShaderProgram sProgram, ShaderProgram sProgramN, int repeat)
        {

            for (int i = 0; i < objeto.Count; i++)
            {
                FVLMesh mesh = objeto[i];
                mesh.Build(sProgram, sProgramN, repeat);

                //Console.WriteLine("BUILD: " + mesh.getName() + " texture: " + mesh.getTexturePath());
                if (mesh.getName() != null)
                {
                    int text = mapeo.find(mesh.getName());

                    if (text == -1)
                    {
                        if (mesh.getTexturePath() != null)
                        {
                            text = CargarTextura(mesh.getTexturePath());
                            mapeo.setClave(text, mesh.getTexturePath(), mesh.getName());
                        }
                    }

                    mesh.setTexture(text);

                    if (mesh.getNormalPath() != null)
                    {
                        text = CargarTextura(mesh.getNormalPath());
                        mesh.setNormalIndex(text);
                    }


                }
                    //Console.WriteLine("objeto rodante:"+i+" "+ mesh.getName());

            }
            Console.WriteLine("finalizando: cantidad de texturas cargadas:" + mapeo.Count());

        }

        


        public void setPosition(Vector3 pos)
        {
            position = pos;
        }

        public void setTextura(int text)
        {
            for (int i = 0; i < objeto.Count; i++)
            {
                objeto[i].setTexture(text);
            }
            tieneTextura = true;


        }


        public void setViewMatrix(Matrix4 v)
        {
            viewMatrix = v;
        }

        public virtual void setModelMatFisica(Matrix4 mod)
        {
            modelMatFisica = mod;
        }

        private int CargarTextura(String imagenTex)
        {
            int texId = GL.GenTexture();//genera identificador para el buffer de textura
            try
            {
                GL.BindTexture(TextureTarget.Texture2D, texId);//pepara buffer de textura


                Bitmap bitmap = new Bitmap(Image.FromFile(imagenTex));//genera un mapa de bits

                BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                                 ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);

                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);


                {
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)All.ClampToBorder);
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)All.ClampToBorder);
                }


                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                        OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
                bitmap.UnlockBits(data);
            }
            catch (System.IO.FileNotFoundException e)
            {
                Console.WriteLine("Cargar Textura No File: "+imagenTex);
            }
            return texId;

        }
        protected int mass = 0;

        public int getMass()
        {
            return mass;
        }

        public virtual void Dibujar(ShaderProgram sProgram, TextureUnit textUnit, int unit, TextureUnit textUnitNormal, int unitNormal)
        {

            for (int i = 0; i < objeto.Count; i++)
            {
                FVLMesh mesh = objeto[i];

                //textura o KD
                if (mesh.getTexturePath() != null || tieneTextura)
                {

                    GL.ActiveTexture(textUnit);
                    GL.BindTexture(TextureTarget.Texture2D, mesh.getTexture());
                    sProgram.SetUniformValue("gSampler", unit);
                    sProgram.SetUniformValue("flagTextura", 1);

                }
                else
                {
                    sProgram.SetUniformValue("flagTextura", 0);
                    sProgram.SetUniformValue("kd", mesh.getKD());

                }

                //para el mapa de normales

                if (mesh.getNormalPath() != null)
                {

                    GL.ActiveTexture(textUnitNormal);
                    GL.BindTexture(TextureTarget.Texture2D, mesh.getNormal());
                    sProgram.SetUniformValue("gSamplerNormalMap", unitNormal);
                    sProgram.SetUniformValue("flagRelieve", 1);

                }
                else
                {
                    sProgram.SetUniformValue("flagRelieve", 0);

                }

                //KS

                sProgram.SetUniformValue("ke", mesh.getKS());



                mesh.Dibujar(sProgram);
            }



            //Console.WriteLine("Finalizando de dibujar " + objeto.Count + " objetos");

        }


        public virtual void DibujarShadows(ShaderProgram sProgram)
        {

            for (int i = 0; i < objeto.Count; i++)
            {
                FVLMesh mesh = objeto[i];

                mesh.DibujarShadows(sProgram);
           }
        }


        public virtual void move(Labo0.fisica fisica) { }


        public void setVertexList(List<Vector3> list)
        {
            vertexList = list;
            for (int i = 0; i < objeto.Count; i++)
            {
                objeto[i].setVertexList(list); 
            }
        }

        public void setNormalList(List<Vector3> list)
        {
            for (int i = 0; i < objeto.Count; i++)
            {
                objeto[i].setNormalList(list);
            }
        }

        public void setTextCoodList(List<Vector2> list)
        {
            for (int i = 0; i < objeto.Count; i++)
            {
                objeto[i].setTextCoodList(list);
            }
        }
        protected int index;
        public void setIndex(int i)
        {
            index = i;
        }

        internal int getIndex()
        {
            return index;
        }

        public List<CGUNS.Meshes.FaceVertexList.FVLMesh> getObjeto()
        {
            return objeto;
        }

        public virtual Vector3 getColision()
        {
            return new Vector3(0);
        }
        public Vector3 getPosition()
        {
            return position;
        }

        public void setColisionBody(TriangleMesh shape)
        {
            //siempre y cuando posiciones sea multiplo de 3
            for (int i = 0; i < objeto.Count; i++)
            {
                Vector3[] posiciones = objeto[i].getPosiciones();
                for(int j = 0; j< posiciones.Length; j += 3)
                {
                    shape.AddTriangle(posiciones[j], posiciones[j+1], posiciones[j+2]);
                }

            }
        }

        public List<Vector3> getVertexList()
        {
            return vertexList;
        }


        


    }



}
