﻿using CGUNS.Meshes.FaceVertexList;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CGUNS.Shaders;
using OpenTK.Graphics.OpenGL;

namespace Labo0.CGUNS.Meshes.FaceVertexList
{
    class Tierra : MeshObject
    {
        public Tierra(String file, String mtl, Vector3 posicionInicial, bool texturado) : base( file, mtl, posicionInicial, texturado)
        {
            modelMat = Matrix4.CreateScale(1);
        }

        public override void Dibujar(ShaderProgram sProgram, TextureUnit textUnit, int unit, TextureUnit textUnitNormal, int unitNormal)
        {
            Vector4 Ka = new Vector4(0.2f, 0.2f, 0.2f, 1);
            Matrix4 mvMatrix = Matrix4.Identity;

            mvMatrix = Matrix4.Mult(modelMat, mvMatrix);
            mvMatrix = Matrix4.Mult(modelMatFisica, mvMatrix);
            sProgram.SetUniformValue("viewMatrix", viewMatrix);
            sProgram.SetUniformValue("modelMat", mvMatrix);

            sProgram.SetUniformValue("n", 1.0f);//coheficiente del aire

            sProgram.SetUniformValue("sigma", 0.3f);

            sProgram.SetUniformValue("ka", Ka);

            //sProgram.SetUniformValue("sigma", 0.3f);


            //sProgram.SetUniformValue("gSampler", 3);
            sProgram.SetUniformValue("flagLuz", 1); //para habilitar o desabilitar la grafica
            sProgram.SetUniformValue("flagTextura", 1); //para habilitar o desabilitar la grafica
            //sProgram.SetUniformValue("flagRelieve", 1); //para habilitar o desabilitar la grafica


            Matrix3 MatNorm = new Matrix3(modelMat);
            sProgram.SetUniformValue("MatrixNormal", MatNorm);


            base.Dibujar(sProgram, textUnit, unit, textUnitNormal,unitNormal);
        }
    }
}
