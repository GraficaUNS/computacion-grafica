﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK; //La matematica

using BulletSharp;
using BulletSharp.SoftBody;
using CGUNS.Cameras;
using CGUNS.Meshes.FaceVertexList;

namespace Labo0
{

    public class fisica
    {

        private BroadphaseInterface broadphase;
        private DefaultCollisionConfiguration collisionConfiguration;
        private CollisionDispatcher dispatcher;
        SequentialImpulseConstraintSolver solver;
        private DiscreteDynamicsWorld dynamicsWorld;
        private RigidBody Rplano;
        private SoftBody S;

        private RigidBody fallRigidBody;
        private Random R;
        private int cantidad;
        private List<Matrix4> listaM = new List<Matrix4>();
        private List<RigidBody> listaRB = new List<RigidBody>();
        private RayResultCallback call;

        public List<Matrix4> ListaM
        {
            get{return listaM;}
            set{ listaM = value;}
        }

        public List<RigidBody> ListaRB
        {
            get { return listaRB;}
            set{ listaRB = value;}
        }

        public int getCantidad()
        {
            return cantidad;
        }
        public void setCantidad(int c)
        {
            cantidad = c;
        }

        public DiscreteDynamicsWorld DynamicsWorld
        {
            get
            {
                return dynamicsWorld;
            }

            set
            {
                dynamicsWorld = value;
            }
        }

        public fisica()
        {
            inicializarMundo();
            R = new Random(17);
        }

        private void inicializarMundo()
        {
            cantidad = 0;
            
            broadphase = new DbvtBroadphase(); //orden n^2
            collisionConfiguration = new DefaultCollisionConfiguration();
            dispatcher = new CollisionDispatcher(collisionConfiguration);
            //solver = new SequentialImpulseConstraintSolver();
            //mundo
            DynamicsWorld = new DiscreteDynamicsWorld(dispatcher, broadphase, null, collisionConfiguration);
            DynamicsWorld.Gravity = new Vector3(0, -10, 0);
            //se agregan los colliders
            //plano estatico 
            CollisionShape groundShape = new StaticPlaneShape(new Vector3(0, 1, 0), 0);
            
            //motion state hace referencia a la matriz con la cual se creará el cuerpo rigido
            DefaultMotionState myMotionState = new DefaultMotionState(Matrix4.CreateTranslation(0, 0, 0));
            
            //el primer parametro es la masa, y el último es la inercia inicial con la cual se creará
            RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(0, myMotionState, groundShape, new Vector3(0, 0, 0));
            //se crea el plano como cuerpo rigido
            Rplano = new RigidBody(rbInfo);
            //finalmente se agrega el plano al mundo
            //DynamicsWorld.AddRigidBody(Rplano);
            
        }
        void eliminar()
        {
            //aca se elimina
            int i;
            for (i = DynamicsWorld.NumConstraints - 1; i >= 0; i--)
            {
                TypedConstraint constraint = DynamicsWorld.GetConstraint(i);
                DynamicsWorld.RemoveConstraint(constraint);
                constraint.Dispose();
            }
            for (i = DynamicsWorld.NumCollisionObjects - 1; i >= 0; i--)
            {
                CollisionObject obj = DynamicsWorld.CollisionObjectArray[i];
                RigidBody body = obj as RigidBody;
                if (body != null && body.MotionState != null)
                {
                    body.MotionState.Dispose();
                }
                DynamicsWorld.RemoveCollisionObject(obj);
                obj.Dispose();
            }

            DynamicsWorld.Dispose();
            broadphase.Dispose();
            if (dispatcher != null)
            {
                dispatcher.Dispose();
            }
            collisionConfiguration.Dispose();

        }
        
        public RigidBody LocalCreateRigidBody(float mass, Matrix4 startTransform, CollisionShape shape)
        {
            bool isDynamic = (mass != 0.0f);

            Vector3 localInertia = Vector3.Zero;
            if (isDynamic)
                shape.CalculateLocalInertia(mass, out localInertia);

            DefaultMotionState myMotionState = new DefaultMotionState(startTransform);

            RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
            RigidBody body = new RigidBody(rbInfo);

            DynamicsWorld.AddRigidBody(body);

            return body;
        }
        public Matrix4 getMatrixModelPlano()
        {
            return Rplano.MotionState.WorldTransform;
        }

        public void reiniciar(int index, Vector3 position, float rotacion)
        {
            RigidBody a = ListaRB[index];
            a.ClearForces();
            a.LinearVelocity = new Vector3(0);
            a.WorldTransform = Matrix4.Mult(Matrix4.CreateRotationY(rotacion),Matrix4.CreateTranslation(position));

        }


        public void corregir(int index)
        {
            RigidBody a = ListaRB[index];
            a.LinearVelocity = Vector3.Transform(new Vector3(0,0,1),listaM[index]);
        }

        //public int agregarCuboRigido(Vector3 colision, Vector3 posicion, int mass)        
        public int agregarCuboRigido(MeshObject obj, Vector3 posicion, int mass, bool terreno)
        {
            
            TriangleMesh objeto = new TriangleMesh();
            cantidad++;

            obj.setColisionBody(objeto);
            // para un cubo
            //CollisionShape shape = new BoxShape(colision);
            if (terreno)
            {
                Matrix4 M = Matrix4.CreateTranslation(posicion);
                ListaM.Add(M);
                DefaultMotionState fallMotionState2 = new DefaultMotionState(M);
                
                CollisionShape shape = new BvhTriangleMeshShape(objeto, true);
                

                Vector3 localInertia2 = shape.CalculateLocalInertia(0);


                RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(0.0f, fallMotionState2, shape, localInertia2);

               

                RigidBody tierra = new RigidBody(rbInfo);


                tierra.Friction = 0.3f;
                tierra.RollingFriction = 0;
                tierra.Restitution = 0.0f;
                
                ListaRB.Add(tierra);
                


                DynamicsWorld.AddRigidBody(tierra);
            }
            else
            {
                CollisionShape shape = new ConvexTriangleMeshShape(objeto, true);


                //TODO rotaciones
                Matrix4 M = Matrix4.CreateTranslation(posicion);
                ListaM.Add(M);
                DefaultMotionState fallMotionState2 = new DefaultMotionState(M);
                Vector3 localInertia2 = shape.CalculateLocalInertia(mass);
                RigidBodyConstructionInfo fallRigidBodyCI2 = new RigidBodyConstructionInfo(1, fallMotionState2, shape, localInertia2);
                fallRigidBody = new RigidBody(fallRigidBodyCI2);


                fallRigidBody.Friction = 0.7f; //El tanque tracciona, no se mueve si no se le indica.
                fallRigidBody.RollingFriction = 0;
                //fallRigidBody.ForceActivationState(ActivationState.DisableDeactivation); //Siempre activo!
                fallRigidBody.Restitution = 0f;

                //para que no se trabe
                fallRigidBody.ForceActivationState(ActivationState.DisableDeactivation);
                ListaRB.Add(fallRigidBody);

                DynamicsWorld.AddRigidBody(fallRigidBody);
            }
            return cantidad - 1;
        }

        public void aplicarFuerza(Vector3 fza, Vector3 position, int index)
        {
            RigidBody R = ListaRB[index];
            position.Y = 0;
            R.ApplyImpulse(fza, position);
            if (fza.Y > 0)
            {

            }
            
            //R.LinearVelocity = new Vector3( 3f, 0, 0);
        }

        public void aplicarVelocidadAngular(Vector3 vel, int index)
        {
            RigidBody R = ListaRB[index];
            R.AngularVelocity += vel;
            //R.LinearVelocity = new Vector3( 3f, 0, 0);
        }

        //Esta funcion frena la velocdad angular del vehiculo
        public void clearForces(int index)
        {
            RigidBody R = ListaRB[index];
           // Console.WriteLine("CLEAR antes" + R.AngularVelocity);

            R.AngularVelocity= R.AngularVelocity*0.9f;
            //Console.WriteLine("CLEAR " + R.AngularVelocity + nuevo);
        }

        int prueba = 0;

        public bool colition(int index, int colider)
        {
            RigidBody R = ListaRB[index];
            RigidBody P = ListaRB[colider];

            R.UserObject = "auto";
            P.UserObject = "otro";
            
            int numManifolds = dynamicsWorld.Dispatcher.NumManifolds;
            for (int i = 0; i < numManifolds; i++)
            {
                PersistentManifold contactManifold = dynamicsWorld.Dispatcher.GetManifoldByIndexInternal(i);
                CollisionObject obA = contactManifold.Body0 as CollisionObject;
                CollisionObject obB = contactManifold.Body1 as CollisionObject;

                int numContacts = contactManifold.NumContacts;
                for (int j = 0; j < numContacts; j++)
                {
                    ManifoldPoint pt = contactManifold.GetContactPoint(j);
                    if (pt.Distance < 0.0f)
                    {
                        Vector3 ptA = pt.PositionWorldOnA;
                        Vector3 ptB = pt.PositionWorldOnB;
                        Vector3 normalOnB = pt.NormalWorldOnB;
                    }
                }
                if (obA.UserObject!=null && obB.UserObject != null)
                {
                    //Console.WriteLine((prueba++) +"YES:"+obA.UserObject.ToString()+" "+ obB.UserObject.ToString());
                    return true;
                }
            }
            return false;
        }

        
       


        internal void agregarSoftBody()
        {

            cantidad++;


        }

        public void dispose()
        {
            DynamicsWorld.Dispose();
        }
        

       
    }



}
