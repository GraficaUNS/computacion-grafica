﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK; //La matematica


namespace CGUNS
{
    class Light
    {
        Vector4 position;
        Vector4 Ia;
        Vector4 Id;
        Vector4 Is;
        float coneAngle;
        Vector3 coneDirection;
        int enabled;

        public Vector4 Position
        {
            get
            {
                return position;
            }
            set
            {
                this.position = value;
            }
        }
        public Vector4 Iambient
        {
            get
            {
                return Ia;
            }
            set
            {
                this.Ia = value;
            }
        }
        public Vector4 Idiffuse
        {
            get
            {
                return Id;
            }
            set
            {
                this.Id = value;
            }
        }
        public Vector4 Ispecular
        {
            get
            {
                return Is;
            }
            set
            {
                this.Is = value;
            }
        }
        public Vector3 ConeDirection
        {
            get
            {
                return coneDirection;
            }
            set
            {
                this.coneDirection = value;
            }
        }
        /// <summary>
        /// Cone Angle in degrees.
        /// </summary>
        public float ConeAngle
        {
            get
            {
                return coneAngle;
            }
            set
            {
                this.coneAngle = value;
            }
        }
        public int Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                this.enabled = value;
            }
        }
        public void Toggle()
        {
            if (Enabled == 0)
            {
                Enabled = 1;
            }
            else
            {
                Enabled = 0;
            }
        }
    }
}
